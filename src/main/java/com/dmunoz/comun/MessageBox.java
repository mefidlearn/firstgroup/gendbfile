package com.dmunoz.comun;

import java.util.Optional;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;

/// Clase para el manejo de mensajes.
public class MessageBox {
	public MessageBox(){
		
	};
	
	public static void showError(String as_title, String as_message) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle(as_title);
		alert.setContentText(as_message);
		alert.showAndWait();
	}

	public static Boolean showYesNo(String as_title, String as_message) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle(as_title);
		alert.setContentText(as_message);
		ButtonType okButton = new ButtonType("Si", ButtonData.YES);
		ButtonType noButton = new ButtonType("No", ButtonData.NO);
		alert.getButtonTypes().setAll(okButton, noButton);
		
		Optional<ButtonType> result = alert.showAndWait();

		// Valida si se seleccionó el boton de confirmacion.
		if (result.isPresent()) {
			if (result.get() == okButton) {
				return true;
			}
		}
		// En caso que no se seleccione el boton ok o se cancele se retorna falso.
		return false;
	}
	
}