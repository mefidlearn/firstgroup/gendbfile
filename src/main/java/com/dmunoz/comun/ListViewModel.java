package com.dmunoz.comun;

public class ListViewModel  {
	
	private Integer Id;
	private String Description;

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public ListViewModel(Integer id, String description) {
		super();
		Id = id;
		Description = description;
	}
	
	@Override
	public String toString() {
		return Id + " - " + Description;
		
	}
}
