package com.dmunoz.gendatafile.services;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dmunoz.comun.MessageBox;
import com.dmunoz.gendatafile.MainContext;
import com.dmunoz.gendatafile.model.Attribute;
import com.dmunoz.gendatafile.model.Entity;
import com.dmunoz.gendatafile.model.Information;
import com.dmunoz.gendatafile.model.Template;

public class GenerateTemplate {
	private final static Logger log = LoggerFactory.getLogger(GenerateTemplate.class);

	public GenerateTemplate() {
	}

	public static void generateMixAndSave(String file, Template template, Information info, Entity entity) {
		if(file == null) {
			MessageBox.showError(MainContext.getAppTitle(), "El archivo de salida no puede ser nulo.");
			return;
		}
		
		if (template == null) {
			MessageBox.showError(MainContext.getAppTitle(), "La plantilla no puede ser nula.");
			return;
		}
		
		if (entity == null) {
			MessageBox.showError(MainContext.getAppTitle(), "La entidad no puede ser nula.");
			return;
		}
		log.info("Entidad: " + entity.getSingularName());
		
		List<String> template_app = processApp(template.getTemplate(), info); 
		List<String> template_ent = processEnt(template_app, entity);
		
		saveFile(file, template_ent);
	}

	private static List<String> processApp(List<String> template, Information info) {
		List<String> out_app = new ArrayList<String>();
		String nlin;
		
		for(String line : template) {
			nlin = line.replace("%APP_TECH%", info.getAppName().toLowerCase());
			nlin = nlin.replace("%APP_AUTH%", info.getAutor());
			nlin = nlin.replace("%APP_LGIN%", info.getLogin());
			out_app.add(nlin);
		}
		
		return out_app;
	}

	private static List<String> processEnt(List<String> template, Entity entity) {
		List<String> out_app = new ArrayList<String>();
		String nlin;
		Integer index_att;
		int index = 0;
		
		while (index < template.size()) {
			Boolean att_process = false;
			nlin = replaceEnt(template.get(index), entity);
			
			// Valida si hay que procesar los atributos de la entidad
			if (nlin.contains("%ATT_") ) {
				String aux = "";
				if (nlin.contains("%ATT_BLCK%")) {
					List<String> block_att = new ArrayList<String>();
					index++;
					while(!template.get(index).contains("%ATT_BLCK%") && (index < template.size())) {
						block_att.add(replaceEnt(template.get(index), entity));
						index++;
					}
					
					for(Attribute attribute : entity.getAllColumns()) {
						index_att = 0;
						for(String blc_line : block_att) {
							aux = replaceAtt(blc_line, attribute, index_att);
							out_app.add(aux);
						}
						index_att++;
					}
					
				} else {
					index_att = 0;
					for(Attribute attribute : entity.getAllColumns()) {
						aux = replaceAtt(nlin, attribute, index_att);
						out_app.add(aux);
						index_att++;
					}	
				}
				att_process = true;
			}
			
			if (!att_process ) {
				out_app.add(nlin);
			}
			index++;
		}
		
		return out_app;
	}

	/**
	 * @param template
	 * @param entity
	 * @param index
	 * @return
	 */
	private static String replaceEnt(String line, Entity entity) {
		String nlin = line;
		nlin = nlin.replace("%ENT_DOMN%", entity.getDomain());
		nlin = nlin.replace("%ENT_SING%", entity.getSingularName());
		nlin = nlin.replace("%ENT_PLRL%", entity.getPluralName());
		nlin = nlin.replace("%ENT_PRFX%", entity.getPrefix());
		nlin = nlin.replace("%ENT_PRFC%", entity.getPrefixCap());
		nlin = nlin.replace("%ENT_NAME%", entity.getName());
		nlin = nlin.replace("%ENT_NAMC%", entity.getNameCap());
		nlin = nlin.replace("%ENT_PKDT%", entity.getPkType());
		nlin = nlin.replace("%ENT_PKTC%", entity.getPkTechCap());
		nlin = nlin.replace("%ENT_PKTH%", entity.getPkTech());
	
		return nlin;
	}

	/**
	 * @param nlin
	 * @param attribute
	 * @return
	 */
	private static String replaceAtt(String nlin, Attribute attribute, Integer index) {
		String att_line = nlin;
		att_line = att_line.replace("%ATT_TECH%", attribute.getTechnicalName());
		att_line = att_line.replace("%ATT_TECC%", attribute.getTechnicalCap());
		att_line = att_line.replace("%ATT_TYPE%", attribute.getDatatypeJava());
		att_line = att_line.replace("%ATT_TYPP%", attribute.getDatatypeProperty());
		att_line = att_line.replace("%ATT_SING%", attribute.getSingularName());
		att_line = att_line.replace("%ATT_DESC%", attribute.getDescription());
		att_line = att_line.replace("%ATT_DFVL%", attribute.getDefaultValue());
		att_line = att_line.replace("%ATT_LENG%", attribute.getLength().toString());
		att_line = att_line.replace("%ATT_INDX%", (index == null) ? "0" : index.toString());
		
		return att_line;
	}

	private static void saveFile(String fileSource, List<String> content) {
		
		try {
			FileWriter file = new FileWriter(fileSource);
			BufferedWriter buffer = new BufferedWriter(file);
			
			// Recorre la lista de registros realizando la grabación.
			for(String line : content) {
				buffer.write(line);
				buffer.newLine();
			}
			
			buffer.flush();
			buffer.close();
			file.close();
			
		} catch (Exception e) {
			log.info("Error grabando archivo: " + fileSource);
		}
	}

}
