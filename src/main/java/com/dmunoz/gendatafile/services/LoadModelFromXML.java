package com.dmunoz.gendatafile.services;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.dmunoz.gendatafile.model.AppModel;
import com.dmunoz.gendatafile.model.Attribute;
import com.dmunoz.gendatafile.model.Entity;
import com.dmunoz.gendatafile.model.Information;


/**
 * Clase que permite cargar la información de un modelo a partir de un archivo XML.
 * @author Mefid
 *
 */

public class LoadModelFromXML {
	private final static Logger log = LoggerFactory.getLogger(LoadModelFromXML.class);
	private static AppModel appModel;

	public LoadModelFromXML() {
	}
	
	public static AppModel readFileXML(String fileXml) {
		Document document;

		try {
			File archivo = new File(fileXml);
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = dbf.newDocumentBuilder();
			document = documentBuilder.parse(archivo);
			document.getDocumentElement().normalize();

		} catch (Exception e) {
			log.info("Excepción cargando modelo: " + fileXml);
			return null;
		}

		initModel();

		NodeList nodesInf = document.getElementsByTagName("INFORMATION");
		LoadInformation(nodesInf.item(0));

		NodeList nodesEnt = document.getElementsByTagName("ENTITIES");
		for (int i = 0; i < nodesEnt.item(0).getChildNodes().getLength(); i++) {
			LoadEntities(nodesEnt.item(0).getChildNodes().item(i));
		}
		
		log.info("Modelo cargado desde [" + fileXml + "] corresponde a: " + 
			appModel.getInformationModel().toString());
		return appModel;
	}
	
	private static void initModel() {
			appModel = new AppModel();
			appModel.setInformationModel(new Information());
	}

	private static void LoadInformation(Node item) {

		Information aux = appModel.getInformationModel();

		if (item.getNodeName().toUpperCase().compareTo("INFORMATION") != 0) {
			aux.setAutor("@AUTOR");
			aux.setLogin("@LOGIN");
		} else {
			NodeList childs = item.getChildNodes();
			for (int i = 0; i < childs.getLength(); i++) {
				if (childs.item(i).getNodeName().toUpperCase().compareTo("APPNAME") == 0) {
					aux.setAppName(childs.item(i).getTextContent());
				}
				
				if (childs.item(i).getNodeName().toUpperCase().compareTo("AUTOR") == 0) {
					aux.setAutor(childs.item(i).getTextContent());
				}

				if (childs.item(i).getNodeName().toUpperCase().compareTo("LOGIN") == 0) {
					aux.setLogin(childs.item(i).getTextContent());
				}

			}
		}
	}

	private static void LoadEntities(Node item) {
		Entity aux = new Entity();

		if (item.getNodeName().toUpperCase().compareTo("ENTITY") == 0) {
			NodeList childs = item.getChildNodes();
			for (int i = 0; i < childs.getLength(); i++) {
				if (childs.item(i).getNodeName().toUpperCase().compareTo("TECHNICALNAME") == 0) {
					aux.setTechnicalName(childs.item(i).getTextContent());
				}

				if (childs.item(i).getNodeName().toUpperCase().compareTo("PREFIX") == 0) {
					aux.setPrefix(childs.item(i).getTextContent());
				}
				if (childs.item(i).getNodeName().toUpperCase().compareTo("DOMAIN") == 0) {
					aux.setDomain(childs.item(i).getTextContent());
				}

				if (childs.item(i).getNodeName().toUpperCase().compareTo("NAMESINGULAR") == 0) {
					aux.setSingularName(childs.item(i).getTextContent());
				}

				if (childs.item(i).getNodeName().toUpperCase().compareTo("NAMEPLURAL") == 0) {
					aux.setPluralName(childs.item(i).getTextContent());
				}

				if (childs.item(i).getNodeName().toUpperCase().compareTo("DESCRIPTION") == 0) {
					aux.setDescription(childs.item(i).getTextContent());
				}

				if (childs.item(i).getNodeName().toUpperCase().compareTo("COLUMNS") == 0) {
					NodeList columns = childs.item(i).getChildNodes();

					for (int j = 0; j < columns.getLength(); j++) {
						Attribute col = new Attribute();
						Boolean keyCol = false;
						Integer value;

						if (columns.item(j).getNodeName().toUpperCase().compareTo("COLUMN") == 0) {

							NodeList attColums = columns.item(j).getChildNodes();

							for (int k = 0; k < attColums.getLength(); k++) {
								if (attColums.item(k).getNodeName().toUpperCase().compareTo("TECHNICALNAME") == 0) {
									col.setTechnicalName(attColums.item(k).getTextContent());
								}
								if (attColums.item(k).getNodeName().toUpperCase().compareTo("NAMESINGULAR") == 0) {
									col.setSingularName(attColums.item(k).getTextContent());
								}
								if (attColums.item(k).getNodeName().toUpperCase().compareTo("DESCRIPTION") == 0) {
									col.setDescription(attColums.item(k).getTextContent());
								}
								if (attColums.item(k).getNodeName().toUpperCase().compareTo("DATATYPE") == 0) {
									col.setDatatype(attColums.item(k).getTextContent());
								}
								if (attColums.item(k).getNodeName().toUpperCase().compareTo("LENGHT") == 0) {
									try {
										value = Integer.valueOf(attColums.item(k).getTextContent());
									} catch (NumberFormatException e) {
										value = 15;
									}
									col.setLength(value);
								}
								if (attColums.item(k).getNodeName().toUpperCase().compareTo("DECIMAL") == 0) {
									try {
										value = Integer.valueOf(attColums.item(k).getTextContent());
									} catch (NumberFormatException e) {
										value = 0;
									}
									col.setPrecision(value);
								}
								if (attColums.item(k).getNodeName().toUpperCase().compareTo("MANDATORY") == 0) {
									col.setMandatory(attColums.item(k).getTextContent().compareToIgnoreCase("Y") == 0 ? true : false);
								}
								if (attColums.item(k).getNodeName().toUpperCase().compareTo("KEY") == 0) {
									keyCol = attColums.item(k).getTextContent().compareToIgnoreCase("Y") == 0 ? true : false; 
								}
							}  // Finaliza el ciclo de valor de atributos de columnas.
							
							// Agrega la columna o la llave a la entidad.
							if (keyCol) {
								aux.addKey(col);
							} else {
								aux.addColumn(col);
							}
							//log.info(col.toString());
						} // Fin de que el nodo sea una columna.
					} // Finaliza el ciclo que recorre columnas.
				} // Fin de el nodo corresponda a las columnas.

			} // Finaliza el ciclo de atributos de la entidad.
			appModel.addEntity(aux);
			
		} // Fin de el nodo corresponda a una entidad.
	}
}
