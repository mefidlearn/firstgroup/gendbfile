package com.dmunoz.gendatafile.services;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoadTemplateFromFile {
	private final static Logger log = LoggerFactory.getLogger(LoadTemplateFromFile.class);
	private static List<String> lst_template;

	public LoadTemplateFromFile() {
	}

	public static List<String> readFile(String fileSource) {
		// Inicializa la plantilla de salida.
		initTemplate();
		
		try {
			// Abre el buffer de lectura del archivo.
			FileReader file = new FileReader(fileSource);
			BufferedReader buffer = new BufferedReader(file);
			String s;
			// Recorre el archivo realizando la lectura.
			while ((s = buffer.readLine()) != null) {
				lst_template.add(s);
			}
			buffer.close();
			file.close();

		} catch (Exception e) {
			log.info("Excepción cargando plantilla: " + fileSource);
		}
		log.info("Cargado: [" + fileSource + "] -  Líneas cargadas: " + lst_template.size());
		return lst_template;
	}

	private static void initTemplate() {
		if (lst_template == null) {
			lst_template = new ArrayList<String>();
		}
		lst_template.clear();
	}

}
