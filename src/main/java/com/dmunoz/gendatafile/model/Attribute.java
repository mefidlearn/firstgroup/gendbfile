package com.dmunoz.gendatafile.model;

import java.util.Formatter;

import com.dmunoz.comun.UtlString;

public class Attribute {
	
	private static final String EMPTY = "";
	private String technicalName;
	private String singularName;
	private String description;
	private String dataType;
	private Integer length;
	private Integer precision;
	private Boolean mandatory;
	
	public String getTechnicalName() {
		return technicalName;
	}

	public String getSingularName() {
		return singularName;
	}

	public String getDescription() {
		return description;
	}

	public String getDatatype() {
		return dataType;
	}

	public Integer getLength() {
		return length;
	}

	public Integer getPrecision() {
		return precision;
	}

	public Boolean getMandatory() {
		return mandatory;
	}
	
	public String getTechnicalCap() {
		return UtlString.capitalize(technicalName);
	}

	public String getDatatypeProperty() {
		return DataTypes.getDataTypeProperty(dataType);
	}

	public String getDatatypeJava() {
		return DataTypes.getDataTypeJava(dataType);
	}
	
	public String getDefaultValue() {
		return DataTypes.getDataTypeDefValue(dataType);
	}

	public void setTechnicalName(String technicalName) {
		this.technicalName = technicalName;
	}

	public void setSingularName(String nameSingular) {
		this.singularName = nameSingular;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDatatype(String datatype) {
		this.dataType = datatype;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	public void setPrecision(Integer precision) {
		this.precision = precision;
	}

	public void setMandatory(Boolean mandatory) {
		this.mandatory = mandatory;
	}

	public Attribute(String technicalName, String singularName, String description, 
			String datatype, Integer length, Integer precision, Boolean mandatory) {
		super();
		this.technicalName = technicalName;	
		this.singularName = singularName;
		this.description = description;
		this.dataType = datatype;
		this.length = length;
		this.precision = precision;
		this.mandatory = mandatory;
	}
	
	public Attribute(String technicalName, String singularName, String description, 
			String datatype, Integer length, Boolean mandatory ) {
		this(technicalName, singularName, description, datatype, length, 0, mandatory);
	}
	
	public Attribute() {
		this(EMPTY, EMPTY, EMPTY, "STRING", 1, false);
	}

	@Override
	public String toString() {
		Formatter fmt = new Formatter();
		Formatter fmtNum = new Formatter();
		String man = this.mandatory ? "*" : " ";
		String dat = "";
		
		if (this.dataType.equals(DataTypes.STRING)) {
			dat = fmtNum.format("%s(%2d)", this.dataType, this.length).toString();	
		}
		if (this.dataType.equals(DataTypes.INTEGER)) {
			dat = fmtNum.format("%s(%2d)", this.dataType, this.length).toString();	
		}
		if (this.dataType.equals(DataTypes.NUMBER)) {
			dat = fmtNum.format("%s(%2d,%d)", this.dataType, this.length, this.precision).toString();	
		}
		if (this.dataType.equals(DataTypes.DATE)) {
			dat = fmtNum.format("%s", this.dataType).toString();	
		}
		
		String aux = fmt.format("%s %-20s %-15s %s", man, 
				this.technicalName, dat, this.singularName).toString();
		fmtNum.close();
		fmt.close();
		
		return aux;
	}

}
