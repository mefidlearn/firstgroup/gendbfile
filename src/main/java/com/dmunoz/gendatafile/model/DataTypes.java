package com.dmunoz.gendatafile.model;

public final class DataTypes {
	
	public static String INTEGER = "INTEGER";
	public static String LONG = "LONG";
	public static String NUMBER = "NUMBER";
	public static String STRING = "STRING";
	public static String DATE = "DATE";
	
	public final static String getDataTypeProperty(String dataType) {
		
		if (dataType.equals(DATE)) {
			return  "ObjectProperty<" + getDataTypeJava(dataType) + ">" ;
		}
		return getDataTypeJava(dataType) + "Property" ;
	}

	public static String getDataTypeJava(String dataType) {
		String dtJava = "";
		
		if (dataType.equals(STRING)) {
			dtJava = "String";
		}
		
		if (dataType.equals(INTEGER)) {
			dtJava = "Integer";
		}
		
		if (dataType.equals(LONG)) {
            dtJava = "Long";
        }
		
		if (dataType.equals(NUMBER)) {
			dtJava = "Double";
		}
		
		if (dataType.equals(DATE)) {
			dtJava = "LocalDate";
		}
		
		return dtJava;
	}

	public static String getDataTypeDefValue(String dataType) {
		String dtValue = "";
		
		if (dataType.equals(STRING)) {
			dtValue = "EMPTY";  // Constante EMPTY definida en la clase.
		}
		
		if (dataType.equals(INTEGER)) {
			dtValue = "0";
		}
		
		if (dataType.equals(LONG)) {
            dtValue = "0";
        }
        
		if (dataType.equals(NUMBER)) {
			dtValue = "0.0";
		}
		
		if (dataType.equals(DATE)) {
			dtValue = "java.time.LocalDate.now()";
		}
		
		return dtValue;
	}

}
