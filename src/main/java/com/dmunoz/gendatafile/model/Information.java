package com.dmunoz.gendatafile.model;

public class Information {
	private String appName;
	private String autor;
	private String login;

	public Information(String appName, String autor, String login) {
		super();
		this.appName = appName;
		this.autor = autor;
		this.login = login;
	}
	
	public String getAppName() {
		return appName;
	}
	
	public String getAutor() {
		return autor;
	}

	public String getLogin() {
		return login;
	}
	
	public void setAppName(String appName) {
		this.appName = appName;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public Information() {
		this("NUEVA APP", "", "");
	}

	@Override
	public String toString() {
		return "App: [" + appName + "] Autor: [" + autor + "] - Login: [" + login + "]";
	}
}
