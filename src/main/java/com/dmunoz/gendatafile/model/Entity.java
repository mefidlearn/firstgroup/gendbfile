package com.dmunoz.gendatafile.model;

import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;

import com.dmunoz.comun.UtlString;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Entity {

	private static final String EMPTY = "";
	private String technicalName;
	private String prefix;
	private String domain;
	private String singularName;
	private String pluralName;
	private String description;
	private Attribute key;
	private List<Attribute> columns;
	private List<Attribute> allColumns;
	
	// Propiedades requeridas para usar en una tabla.
	private StringProperty singularNameProp;
	private BooleanProperty selected;

	public String getTechnicalName() {
		return technicalName;
	}

	public String getPrefix() {
		return prefix;
	}

	public String getDomain() {
		return domain;
	}

	public String getSingularName() {
		return singularName;
	}

	public String getPluralName() {
		return pluralName;
	}

	public String getDescription() {
		return description;
	}

	public Attribute getKey() {
		return key;
	}

	public List<Attribute> getColumns() {
		return columns;
	}

	public List<Attribute> getAllColumns() {
		return allColumns;
	}

	public StringProperty getSingularNameProperty() {
		return this.singularNameProp;
	}
	
	public BooleanProperty getSelectedProperty( ) {
		return this.selected;
	}
	
	public String getName() {
		String aux;
		
		if (technicalName.length() > 4) {
			aux = technicalName.substring(4);
		} else {
			aux = technicalName;
		}
		
		return aux.toLowerCase();
	}

	public String getNameCap() {
		return UtlString.capitalize(getName()) ;
	}
	
	public String getPrefixCap() {
		return UtlString.capitalize(prefix);
	}

	public String getPkType() {
		if (this.key == null) {
			return "Object";
		}

		return this.key.getDatatypeJava();
	}

	public CharSequence getPkTech() {
		if (this.key == null) {
			return "Id";
		}

		return this.key.getTechnicalName();
	}

	public CharSequence getPkTechCap() {
		if (this.key == null) {
			return "Id";
		}

		return this.key.getTechnicalCap();
	}
	
	
	public Boolean getSelected() {
		return this.selected.getValue();
	}
	
	public void setTechnicalName(String technicalName) {
		this.technicalName = technicalName;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public void setSingularName(String singularName) {
		this.singularName = singularName;
		this.singularNameProp.set(this.singularName);
	}

	public void setPluralName(String pluralName) {
		this.pluralName = pluralName;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setKey(Attribute key) {
		this.key = key;
	}

	public void setColumns(List<Attribute> columns) {
		this.columns = columns;
	}

	public void setAllColumns(List<Attribute> allColumns) {
		this.allColumns = allColumns;
	}
	
	public void setSelected(Boolean selected) {
		this.selected.set(selected);
	}
	
	public Entity(String technicalName, String prefix, String domain, String singularName, String pluralName, String description) {
		super();
		this.technicalName = technicalName;
		this.prefix = prefix;
		this.domain = domain;
		this.singularName = singularName;
		this.pluralName = pluralName;
		this.description = description;
		// Propiedades para uso en tabla.
		this.singularNameProp = new SimpleStringProperty();
		this.selected = new SimpleBooleanProperty();
		this.singularNameProp.set(this.singularName);
		this.selected.set(false);
	}

	public Entity() {
		this(EMPTY, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY);
	}

	@Override
	protected void finalize() throws Throwable {
		if (allColumns != null) {
			this.allColumns.clear();
		}

		if (columns != null) {
			this.columns.clear();
		}

		this.allColumns = null;
		this.columns = null;
		this.key = null;
		super.finalize();
	}

	public void addColumn(Attribute attribute) {
		if (attribute != null) {
			if (this.columns == null) {
				this.columns = new ArrayList<Attribute>();
			}
			if (this.allColumns == null) {
				this.allColumns = new ArrayList<Attribute>();
			}

			this.columns.add(attribute);
			this.allColumns.add(attribute);
		}
	}

	public void addColumn(String technicalName, String nameSingular, String description, String datatype,
			Integer length, Integer precision, Boolean mandatory) {
		this.addColumn(new Attribute(technicalName, nameSingular, description, datatype, length, precision, mandatory));
	}

	public void addColumn(String technicalName, String nameSingular, String description, String datatype,
			Integer length, Boolean mandatory) {
		this.addColumn(new Attribute(technicalName, nameSingular, description, datatype, length, mandatory));
	}

	public void addKey(Attribute key) {
		if ((key != null) && (this.key == null)) {
			if (this.allColumns == null) {
				this.allColumns = new ArrayList<Attribute>();
			}
			this.key = key;
			this.allColumns.add(0, key);
		}
	}

	public void addKey(String technicalName, String nameSingular, String description, String datatype, Integer length) {
		this.addKey(new Attribute(technicalName, nameSingular, description, datatype, length, true));
	}

	@Override
	public String toString() {
		StringBuilder aux = new StringBuilder();
		Formatter fmt = new Formatter();
		String ent = fmt.format("%s (%s) \n", this.technicalName, this.singularName).toString();
		fmt.close();

		aux.append(ent);

		if (this.key != null) {
			ent = "  pk " + this.key.toString() + "\n";
			aux.append(ent);
		}

		if (this.columns != null) {
			for (Attribute att : this.columns) {
				ent = "     " + att.toString() + "\n";
				aux.append(ent);
			}
		}

		return aux.toString();
	}

}
