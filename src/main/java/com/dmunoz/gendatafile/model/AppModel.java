package com.dmunoz.gendatafile.model;

import java.util.ArrayList;
import java.util.List;

public class AppModel {

	private Information informationModel;
	private List<Entity> entities;

	public Information getInformationModel() {
		return informationModel;
	}

	public List<Entity> getEntities() {
		return entities;
	}

	public void setInformationModel(Information informationModel) {
		this.informationModel = informationModel;
	}

	public void setEntities(List<Entity> entities) {
		this.entities = entities;
	}

	public AppModel(Information informationModel) {
		super();
		this.informationModel = informationModel;
	}

	public AppModel() {
	}

	public void addEntity(Entity entity) {
		if (entity != null) {
			if (this.entities == null) {
				this.entities = new ArrayList<Entity>();
			}
			
			this.entities.add(entity);
		}
	}

	@Override
	public String toString() {
		StringBuilder aux = new StringBuilder();
		if (this.informationModel != null) {
			aux.append(this.informationModel.toString());
		}
		else {
			aux.append("Sin información.");
		}
		
		if (this.entities != null) {
			aux.append(" - Entidades: " + this.entities.size());
		}
		else {
			aux.append(" - Entidades: " + "0");
		}
		return aux.toString();
	}

	@Override
	protected void finalize() throws Throwable {
		if (this.entities != null) {
			this.entities.clear();
		}
		
		this.entities = null;
		this.informationModel = null;
		
		super.finalize();
	}
}
