package com.dmunoz.gendatafile.model;

import java.util.ArrayList;
import java.util.List;

public class Template {
	
	private String name;
	private List<String> template;

	public Template() {
		this.name = "Nueva plantilla";
		this.template = new ArrayList<String>();
	}

	public Template(String name, List<String> template) {
		super();
		this.name = name;
		this.template = template;
	}

	public String getName() {
		return name;
	}

	public List<String> getTemplate() {
		return template;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setTemplate(List<String> template) {
		this.template = template;
	}

	@Override
	public String toString() {
		return this.name + " - Líneas: " + this.template.size();
	}

}
