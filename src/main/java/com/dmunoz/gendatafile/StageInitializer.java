package com.dmunoz.gendatafile;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.dmunoz.gendatafile.StartGenDbFileApp.StageReadyEvent;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

@Component
@Configurable
public class StageInitializer implements ApplicationListener<StageReadyEvent> {

	private ApplicationContext applicationContext;
	private String applicationTitle;
	private BorderPane rootLayout;

	StageInitializer(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
		this.applicationTitle = "Generador de aplicaciones";
		
		MainContext.setApplicationContext(this.applicationContext);
		MainContext.setAppTitle(this.applicationTitle);
	}

	@Override
	public void onApplicationEvent(StageReadyEvent event) {
		try {
			Stage stage = event.getStage();
			MainContext.setPrimaryStage(stage);

			stage.setTitle(this.applicationTitle);
			FXMLLoader loader = new FXMLLoader(getClass().getResource("view/MainWindow.fxml"));
			loader.setControllerFactory(applicationContext::getBean);
			rootLayout = (BorderPane) loader.load();
			
			// Show the scene containing the root layout.
			Scene scene = new Scene(rootLayout);
			stage.setScene(scene);
			stage.setResizable(false);			
			stage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}