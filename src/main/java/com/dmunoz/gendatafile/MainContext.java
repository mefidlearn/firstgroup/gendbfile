package com.dmunoz.gendatafile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.dmunoz.gendatafile.model.AppModel;
import com.dmunoz.gendatafile.model.Template;

import javafx.stage.Stage;

public class MainContext {
	private final static Logger log = LoggerFactory.getLogger(MainContext.class);
	private static ApplicationContext applicationContext;
	private static Stage primaryStage;
	private static String appTitle;
	private static String path;
	private static Template template;
	private static AppModel appModel;

	public static String getAppTitle() {
		return appTitle;
	}

	public static void setAppTitle(String appTitle) {
		MainContext.appTitle = appTitle;
	}

	public MainContext() {
	}

	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	public static void setApplicationContext(ApplicationContext applicationContext) {
		MainContext.applicationContext = applicationContext;
	}

	public static Stage getPrimaryStage() {
		return primaryStage;
	}

	public static void setPrimaryStage(Stage primaryStage) {
		MainContext.primaryStage = primaryStage;
	}

	public static Template getTemplate() {
		return template;
	}

	public static void setTemplate(Template template) {
		MainContext.template = template;
		if (template!= null) {
			log.info(template.toString());
		}
	}

	public static AppModel getAppModel() {
		return appModel;
	}

	public static void setAppModel(AppModel appModel) {
		MainContext.appModel = appModel;
		if (appModel != null) {
			log.info(appModel.toString());
		}
	}

	public static String getPath() {
		return path;
	}

	public static void setPath(String path) {
		MainContext.path = path;
	}

}
