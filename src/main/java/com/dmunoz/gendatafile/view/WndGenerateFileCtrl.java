package com.dmunoz.gendatafile.view;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.dmunoz.comun.ListViewModel;
import com.dmunoz.comun.MessageBox;
import com.dmunoz.gendatafile.MainContext;
import com.dmunoz.gendatafile.model.Entity;
import com.dmunoz.gendatafile.services.GenerateTemplate;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.layout.AnchorPane;
import javafx.stage.DirectoryChooser;
import javafx.util.StringConverter;

@Component
@Scope("prototype")
public class WndGenerateFileCtrl extends BaseCtrl {

	private final static Logger log = LoggerFactory.getLogger(WndGenerateFileCtrl.class);

	@FXML
	private AnchorPane layout;
	@FXML
	private TextField txt_template;
	@FXML
	private TextField txt_path;
	@FXML
	private TableView<Entity> entTable;
	@FXML
	private TableColumn<Entity, Boolean> selectedCol;
	@FXML
	private TableColumn<Entity, String> singularNameCol;
	@FXML
	private TextField txt_prefix;
	@FXML
	private ComboBox<ListViewModel> txt_variable;
	@FXML
	private TextField txt_sufix;
	@FXML
	private ProgressBar pgb_progress;
	@FXML
	private Label lbl_progress;

	private ObservableList<Entity> AllEnts = FXCollections.observableArrayList();
	private ObservableList<ListViewModel> Variables = FXCollections.observableArrayList();

	//private String pathBase;
	private String path;
	private String pathNotSel;
	private Entity ModelNot;
	// private Integer progreso;

	public WndGenerateFileCtrl() {
		//this.pathBase = "D:\\Projects\\Template\\";
		this.pathNotSel = "C:/";
		this.ModelNot = new Entity();
		this.ModelNot.setSingularName("No hay entidades o no se ha cargado el modelo.");
		// progreso = 0;
		
		//Metodo temporal para cargar la información para pruebas
		//loadDataTest();
	}
/*
	private void loadDataTest() {
		//String template_file = "D:\\Mefid\\Downloads\\Template\\04-template_serviceImpl.txt";
		//String template_file = this.pathBase +  "07-template_viewSin.txt";
		String template_file = this.pathBase +  "08-template_viewSinCtrl.txt";
		String xml_file = this.pathBase +  "workcontrol.xml";
		// Carga de plantilla
		Template template = new Template();
		//template.setName("ServiceImpl.java");
		//template.setName("WndSin.fxml");
		template.setName("WndSinCtrl.java");
		template.setTemplate(LoadTemplateFromFile.readFile(template_file));
		AppModel appModel = LoadModelFromXML.readFileXML(xml_file);
		
		MainContext.setTemplate(template);
		MainContext.setAppModel(appModel);
		MainContext.setPath(this.pathBase + "Generate");
	}
	*/

	@FXML
	private void initialize() {
		String aux_template = (MainContext.getTemplate() != null) ? MainContext.getTemplate().getName()
				: "Plantilla no ha sido cargada.";
		this.txt_template.setText(aux_template);
		// Sugerencia del sufijo con base en el nombre de plantilla.
		this.txt_sufix.setText(aux_template); 

		// Inicializa la ruta de salida con base en la ruta de la plantilla.
		this.path = (MainContext.getPath() != null) ? MainContext.getPath() : this.pathNotSel;
		this.txt_path.setText(this.path);

		this.selectedCol.setCellFactory(CheckBoxTableCell.forTableColumn(this.selectedCol));
		this.selectedCol.setCellValueFactory(cellData -> cellData.getValue().getSelectedProperty());
		this.singularNameCol.setCellValueFactory(cellData -> cellData.getValue().getSingularNameProperty());

		// Carga las entidades a la lista.
		if (MainContext.getAppModel() != null) {
			if (MainContext.getAppModel().getEntities() != null) {
				for (Entity entity : MainContext.getAppModel().getEntities()) {
					this.AllEnts.add(entity);
				}

			} else {
				this.AllEnts.add(this.ModelNot);
			}
		} else {
			this.AllEnts.add(this.ModelNot);
		}
		
		this.entTable.setItems(this.AllEnts);
		this.Variables.add(new ListViewModel(1, "Nombre técnico"));
		this.Variables.add(new ListViewModel(2, "Prefijo"));
		this.Variables.add(new ListViewModel(3, "Prefijo capitalizado"));
		this.txt_variable.setItems(this.Variables);

		// Inicializa el combo de Variables
		this.txt_variable.setConverter(new StringConverter<ListViewModel>() {
			@Override
			public String toString(ListViewModel object) {
				return (object != null) ? object.getDescription() : "";
			}

			@Override
			public ListViewModel fromString(String data) {
				return txt_variable.getItems().stream().filter(ap -> ap.getDescription().equals(data)).findFirst()
						.orElse(null);
			}
		});

		this.lbl_progress.setText("");
	}

	@FXML
	public void btnPathAction(ActionEvent event) {
		DirectoryChooser pathChooser = new DirectoryChooser();
		pathChooser.setTitle("Seleccionar directorio de salida");

		if (this.path == null) {
			this.path = this.pathNotSel;
		}

		File defaultDirectory = new File(this.path);
		pathChooser.setInitialDirectory(defaultDirectory);

		// Obtener el directorio seleccionado.
		File file = pathChooser.showDialog(stage);

		// Establece path a la variable respectiva.
		if (file != null) {
			this.path = file.getAbsolutePath();
			this.txt_path.setText(this.path);
		} else {
			this.txt_path.setText(this.pathNotSel);
		}
	}

	@FXML
	public void btnGenerateAction(ActionEvent event) {
		/*
		 * double prog; String lbl_prog;
		 * 
		 * this.progreso += 1; prog = (double) this.progreso / 20; lbl_prog =
		 * "Procesando: " + this.progreso.toString() + " de 20";
		 * 
		 * this.pgb_progress.setProgress(prog); this.lbl_progress.setText(lbl_prog);
		 */

		StringBuilder aux = new StringBuilder();
		ListViewModel var = this.txt_variable.getSelectionModel().getSelectedItem();
		Integer entities = new Integer(0);
		Integer current = new Integer(0);

		Integer variable = 0;

		aux.append("Directorio: " + this.path);
		aux.append("\nEntidades:  ");
		for (Entity entity : this.AllEnts) {
			if (entity.getSelected()) {
				aux.append("\n    " + entity.getSingularName());
				entities += 1;
			}
		}
		aux.append("\nTotal Entidades: " + entities.toString());
		aux.append("\nPrefijo: " + this.txt_prefix.getText());
		if (var != null) {
			aux.append("\nVariable: " + var.getId() + " - " + var.getDescription());
			variable = var.getId();
		} else {
			aux.append("\nVariable: No seleccionada.");
		}
		aux.append("\nSufijo: " + this.txt_sufix.getText());

		if (entities == 0) {
			MessageBox.showError(MainContext.getAppTitle(), "No hay entidades para procesar.");
		}

		for (Entity entity : this.AllEnts) {
			if (entity.getSelected()) {
				StringBuilder out = new StringBuilder();
				String lbl_prog;
				double prog;

				current += 1;
				prog = (double) current / entities;
				lbl_prog = "Procesando: " + current.toString() + " de " + entities.toString();
				this.pgb_progress.setProgress(prog);
				this.lbl_progress.setText(lbl_prog);
				// this.layout.
				log.info(lbl_prog);

				out.append(this.txt_prefix.getText());
				// Valida si la parte variable del nombre es nombre tecnico (1), prefijo (2).
				// Prefijo capitalizado (3).
				switch (variable) {
				case 1:
					out.append(entity.getTechnicalName().toLowerCase());
					break;
				case 2:
					out.append(entity.getPrefix());
					break;
				case 3:
					out.append(entity.getPrefixCap());
					break;

				default:
					break;
				}

				out.append(this.txt_sufix.getText());

				GenerateTemplate.generateMixAndSave(this.path + "\\" + out.toString(), MainContext.getTemplate(),
						MainContext.getAppModel().getInformationModel(), entity);

				out = null;
			}
		}

	}

}
