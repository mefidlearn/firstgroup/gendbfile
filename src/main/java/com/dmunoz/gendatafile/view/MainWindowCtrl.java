package com.dmunoz.gendatafile.view;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.dmunoz.gendatafile.MainContext;
import com.dmunoz.gendatafile.model.AppModel;
import com.dmunoz.gendatafile.model.Template;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.stage.Stage;

@Component
@Scope("prototype")
public class MainWindowCtrl {
	private final static Logger log = LoggerFactory.getLogger(MainWindowCtrl.class);

	public MainWindowCtrl() {
	}

	@FXML
	private void initialize() {
	}
	
	@FXML
	private void mnuExitAction(ActionEvent event) {
		System.exit(0);
	}
	
	@FXML
	private void mnuParameterAction(ActionEvent event) {
		// TODO implementar parametros.
	}
	
	@FXML
	private void mnuLoadModelAction(ActionEvent event) {
		log.info("Cargando: WndLoadEntities.fxml");
		MainContext.setAppModel(null);
		Stage stage = new Stage(); 
		WndLoadEntitiesCtrl controller = 				
			(WndLoadEntitiesCtrl) UtlWindow.initFormCtrl("view/WndLoadEntities.fxml", "Cargar entidades", stage);
		stage.showAndWait();

		if (controller != null) {
			AppModel appModel = controller.getAppModel();
			MainContext.setAppModel(appModel);
		}
	}
	
	@FXML
	private void mnuLoadTemplateAction(ActionEvent event) {
		log.info("Cargando: WndLoadTemplate.fxml");
		MainContext.setTemplate(null);
		Stage stage = new Stage();
		WndLoadTemplateCtrl controller = 
				(WndLoadTemplateCtrl) UtlWindow.initFormCtrl("view/WndLoadTemplate.fxml", "Cargar plantilla", stage);
		stage.showAndWait();

		if (controller != null) {
			Template template = controller.getTemplate();
			MainContext.setTemplate(template);
			MainContext.setPath(controller.getPath());
		}
	}
	
	@FXML
	private void mnuGenerateFilesAction(ActionEvent event) {
		log.info("Cargando: WndLoadTemplate.fxml");
		Stage stage = new Stage();
		/* WndGenerateFileCtrl controller = 
				(WndGenerateFileCtrl)*/
		UtlWindow.initFormCtrl("view/WndGenerateFile.fxml", "Generar archivos", stage);
		stage.showAndWait();

	}

}
