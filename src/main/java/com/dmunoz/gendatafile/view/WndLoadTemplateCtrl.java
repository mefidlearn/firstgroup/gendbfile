package com.dmunoz.gendatafile.view;

import java.io.File;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.dmunoz.gendatafile.model.Template;
import com.dmunoz.gendatafile.services.LoadTemplateFromFile;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;

@Component
@Scope("prototype")
public class WndLoadTemplateCtrl extends BaseCtrl {

	//private final static Logger log = LoggerFactory.getLogger(WndLoadEntitiesCtrl.class);
	@FXML
	private TextField txt_name;
	@FXML
	private TextField txt_template;
	@FXML
	private Label lbl_lines;
	
	private Template template;
	private String path;
	
	@FXML
	public void btnTemplateAction(ActionEvent event) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Seleccionar la plantilla");

		// Agregar filtros para facilitar la busqueda
		fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("TXT", "*.txt"));

		// Obtener el archivo seleccionado.
		File file = fileChooser.showOpenDialog(stage);

		// Procesar el archivo seleccionado.
		if (file != null) {
			List<String> lst_template;
			
			if (this.template == null) {
				this.template = new Template();
			}
			this.txt_template.setText(file.getAbsolutePath());
			this.path = file.getParent();
			lst_template = LoadTemplateFromFile.readFile(this.txt_template.getText());
			
			this.template.setName(this.txt_name.getText());
			this.template.setTemplate(lst_template);
			
			this.lbl_lines.setText("          Líneas: (" + lst_template.size() + ")");
		}
	}
	
	@FXML
	public void btnCloseAction(ActionEvent event) {
		this.close();
	}
	
	public WndLoadTemplateCtrl() {
	}
	
	public Template getTemplate() {
		return template;
	}
	
	public void setTemplate(Template template) {
		this.template = template;
	}

	@Override
	protected void finalize() throws Throwable {
		if (this.template != null) {
			this.template = null;
		}
		
		super.finalize();
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
}
