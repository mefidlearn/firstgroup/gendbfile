package com.dmunoz.gendatafile.view;

import javafx.stage.Stage;

public class BaseCtrl {
	protected Stage stage;

	public BaseCtrl() {
	}
	
	public void setStage(Stage stage) {
		this.stage = stage;
	}
	
	public Stage getStage() {
		return this.stage;
	}

	protected void close() {
		stage.close();
	}


}
