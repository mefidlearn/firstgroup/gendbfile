package com.dmunoz.gendatafile.view;

import java.io.File;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.dmunoz.gendatafile.MainContext;
import com.dmunoz.gendatafile.model.AppModel;
import com.dmunoz.gendatafile.model.Entity;
import com.dmunoz.gendatafile.services.LoadModelFromXML;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;

@Component
@Scope("prototype")
public class WndLoadEntitiesCtrl extends BaseCtrl {
	//private final static Logger log = LoggerFactory.getLogger(WndLoadEntitiesCtrl.class);
	@FXML
	private TextField txt_model;
	@FXML
	private ListView<String> lst_entities;
	@FXML
	private TextArea txt_entity;

	private AppModel appModel;

	@FXML
	public void lstSelectedAction(Event event) {
		Integer index = lst_entities.getSelectionModel().getSelectedIndex();
		this.txt_entity.setText(this.appModel.getEntities().get(index).toString());
	}

	@FXML
	public void btnCloseAction(ActionEvent event) {
		this.close();
	}	

	@FXML
	public void btnModelAction(ActionEvent event) {
		File aux = (MainContext.getPath() != null) ? new File(MainContext.getPath()) : null;
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Seleccionar el modelo");
		if (aux != null) {
			fileChooser.setInitialDirectory(aux);
		}

		// Agregar filtros para facilitar la busqueda
		fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("XML", "*.xml"));

		// Obtener el archivo seleccionado.
		File file = fileChooser.showOpenDialog(stage);

		// Cargar el archivo.
		if (file != null) {
			this.txt_model.setText(file.getAbsolutePath());
			this.appModel = LoadModelFromXML.readFileXML(this.txt_model.getText());
			this.SetEntities();
		}
	}
	
	public WndLoadEntitiesCtrl() {
	}

	public AppModel getAppModel() {
		return appModel;
	}

	public void setAppModel(AppModel appModel) {
		this.appModel = appModel;
	}
		
	private void SetEntities() {
		if (this.appModel != null) {
		    this.lst_entities.getItems().clear();
			if (this.appModel.getEntities() != null) {
				for (Entity entity : this.appModel.getEntities()) {
					this.lst_entities.getItems().add(entity.getSingularName());					
				}
			}
		}
	}

	@Override
	protected void finalize() throws Throwable {
		if (this.appModel != null) {
			this.appModel = null;
		}
		super.finalize();
	}
}
