package com.dmunoz.gendatafile.view;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dmunoz.gendatafile.MainContext;
import com.dmunoz.gendatafile.StartGenDbFileApp;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * @author mefid
 *
 */
public class UtlWindow {
	private final static Logger log = LoggerFactory.getLogger(UtlWindow.class);
	/**
	 * 
	 */
	public UtlWindow() {
	}

	public static FXMLLoader getLoader(String as_winResource) {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(StartGenDbFileApp.class.getResource(as_winResource));
		loader.setControllerFactory(MainContext.getApplicationContext()::getBean);

		return loader;
	}

	public static Stage getStage(FXMLLoader ao_loader, String as_title) throws IOException {
		AnchorPane mainPane = (AnchorPane) ao_loader.load();
		Scene scene = new Scene(mainPane);
		Stage dialogStage = new Stage();
		dialogStage.setTitle(as_title);
		dialogStage.initModality(Modality.WINDOW_MODAL);
		dialogStage.initOwner(MainContext.getPrimaryStage());
		dialogStage.setScene(scene);

		return dialogStage;
	}
	
	public static BaseCtrl initFormCtrl(String resource, String title, Stage dialogStage) {
		try {
			FXMLLoader loader = getLoader(resource);
			AnchorPane paneLayout = (AnchorPane) loader.load();
			Scene scene = new Scene(paneLayout);
			dialogStage.setTitle(title);
			dialogStage.initModality(Modality.WINDOW_MODAL);
			BaseCtrl controller = loader.getController();
			controller.setStage(dialogStage);
			
			// Muestra la escena conteniendo el layout raiz. 
			dialogStage.setScene(scene);
			dialogStage.setResizable(false);
			return controller;
			
		} catch (Exception e) {
			log.info("No se pudo cargar la forma: " + resource);
			return null;
		}
	}
}
