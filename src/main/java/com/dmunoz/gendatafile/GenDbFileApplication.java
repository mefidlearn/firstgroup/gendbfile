package com.dmunoz.gendatafile;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import javafx.application.Application;

@SpringBootApplication
@ComponentScan(basePackages = "com.dmunoz.gendatafile")
public class GenDbFileApplication {

	public static void main(String[] args) {
		//SpringApplication.run(GenDbFileApplication.class, args);
		Application.launch(StartGenDbFileApp.class, args);
	}

}
